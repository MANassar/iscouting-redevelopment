//
//  ArraySelectorTableViewController.swift
//  iScouting
//
//  Created by Mohamed Nassar on 09/04/2017.
//  Copyright © 2017 Mohamed Nassar. All rights reserved.
//

import Foundation
import UIKit

class ArraySelectorTableViewController:UITableViewController
{
    var delegate:ArraySelectorTableViewControllerDelegate?
    var baseArray:[String]?
    var localizedArray = [String]()
    var reciever:AnyObject?
    
    override func viewDidLoad()
    {
        if (baseArray != nil)
        {
            for originalString in baseArray! {
                localizedArray.append(LanguageManager.localizeString(stringToLocalize: originalString))
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return localizedArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellID = "ArraySelectorCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        cell.textLabel?.text = localizedArray[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let selectedString = localizedArray[indexPath.row]
        delegate?.arraySelectorTableVCDidSelect(arraySelectorVC: self, selection: selectedString)
        self.dismiss(animated: true, completion: nil)
    }
}

protocol ArraySelectorTableViewControllerDelegate {
    func arraySelectorTableVCDidSelect(arraySelectorVC:ArraySelectorTableViewController, selection:String)
}
