//
//  Attribute.swift
//  iScouting
//
//  Created by Mohamed Nassar on 15/06/2017.
//  Copyright © 2017 Mohamed Nassar. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

enum AttributeType:Int {
    case Physical = 0
    case Tactical = 1
    case Technical = 2
    case Psycho_Social = 3
}

class Attribute:Object
{
    dynamic var id:Int = 0
    
    dynamic var defaultName = ""
    dynamic var customName = ""
    dynamic var value:Float = 0.0
    dynamic var attributeTypeInt:Int = -1
    dynamic var hasCustomName = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(defaultName:String, value:Float, type:AttributeType, id:Int)
    {
        self.init()
        self.defaultName = defaultName
        self.customName = defaultName //For now. We can override that later
        self.value = value
        self.attributeTypeInt = type.rawValue
        self.id = id
    }
}
