//
//  Match.swift
//  iScouting
//
//  Created by Mohamed Nassar on 15/06/2017.
//  Copyright © 2017 Mohamed Nassar. All rights reserved.
//

import Foundation
import RealmSwift

class Match//:Object
{
    dynamic var date:Date?
    dynamic var homeTeam = ""
    dynamic var awayTeam = ""
    dynamic var competition = ""
    dynamic var weather = ""
    dynamic var conditions = ""
    dynamic var scoreHome = 0
    dynamic var scoreAway = 0
    dynamic var pitchCondition:String?
}
