//
//  Player.swift
//  iScouting
//
//  Created by Mohamed Nassar on 7/2/16.
//  Copyright © 2016 Mohamed Nassar. All rights reserved.
//

import UIKit
import RealmSwift

class Player//: Object
{
    dynamic var id = -1
//    var playerImage:UIImage?
    dynamic var playerImageName = ""
    
    //Old Player Personal Details A
    
    dynamic var name:String = "First Name"
    dynamic var surname:String = "Surname"
    dynamic var nationality:String = "Human"
    dynamic var dateOfBirth:Date = Date()
    dynamic var position:String = "N/A"
    dynamic var alternatePosition:String = "N/A"
    
    //Old Personal Details B
    dynamic var height:Float = 0.0
    dynamic var weight:Float = 0.0
    dynamic var build:String = "N/A"
    dynamic var foot:String = "N/A"
    dynamic var currentClub:String = "World Squad"
    dynamic var previousClub:String = "Mars Squad"
    dynamic var shirtNumber:Int = -1
    dynamic var contractExpiry:Date = Date()
    dynamic var injuries:String = "None" //Should be 30 chars
    dynamic var videoURLString:String? = ""
    dynamic var salaryAmount:Int = 0
    dynamic var salaryCurrency:String = "$"
    dynamic var playerAgent:String = "Unknown"
    
//    var physicalAttributesArray:[Attribute]
//    var tacticalAttributesArray:[Attribute]
//    var technicalAttributesArray:[Attribute]
//    var psychoSocialAttributesArray:[Attribute]
    
//    override static func primaryKey() -> String? {
//        return "id"
//    }
    
//    convenience init(id:Int) {
//        
//        self.id = id
//        self.physicalAttributesArray = [Attribute(defaultName: "Strength", value: 0.0, type: AttributeType.Physical, id: 0),
//                                        Attribute(defaultName: "Power", value: 0.0, type: AttributeType.Physical, id: 1),
//                                        Attribute(defaultName: "Pace", value: 0.0, type: AttributeType.Physical, id: 2),
//                                        Attribute(defaultName: "Stamina", value: 0.0, type: AttributeType.Physical, id: 3),
//                                        Attribute(defaultName: "Agility", value: 0.0, type: AttributeType.Physical, id: 4),
//                                        Attribute(defaultName: "Flexibility", value: 0.0, type: AttributeType.Physical, id: 5),
//                                        Attribute(defaultName: "Coordination", value: 0.0, type: AttributeType.Physical, id: 6),
//                                        Attribute(defaultName: "Work Rate", value: 0.0, type: AttributeType.Physical, id: 7),
//                                        Attribute(defaultName: "Reaction", value: 0.0, type: AttributeType.Physical, id: 8),
//                                        Attribute(defaultName: "Acceleration", value: 0.0, type: AttributeType.Physical, id: 9)]
//        
//        var tacticalAttributesArray = ["Positioning", "Anticipating", "Decision making", "Vision", "Awareness", "Off the ball", "1 vs 1 Attack", "1 vs 1 Defence", "Set pieces attack", "Set pieces defence", ""]
//    }
}






