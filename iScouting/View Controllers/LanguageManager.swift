//
//  LanguageManager.swift
//  iScouting
//
//  Created by Mohamed Nassar on 08/08/2016.
//  Copyright © 2016 Mohamed Nassar. All rights reserved.
//

import Foundation
import UIKit

/*
 This class handles the dynamic translation of strings across all languages.
 */

class LanguageManager
{
    static let sharedManager = LanguageManager()
    
    class func localizeString(stringToLocalize:String) -> String
    {
        // Get the corresponding bundle path.
        let selectedLanguage = self.getLanguage()
        let path = Bundle.main.path(forResource: selectedLanguage, ofType: "lproj")
        
        // Get the corresponding localized string.
        let languageBundle = Bundle(path: path!)
        return languageBundle!.localizedString(forKey: stringToLocalize, value: "", table: nil)
    }
 
    class func getLanguage() -> String
    {
        return (UserDefaults.standard.object(forKey: "AppleLanguages") as! [String])[0]
    }
    
    class func localizeUI(parentView:UIView)
    {
        for view:UIView in parentView.subviews
        {
//            debugPrint(view)
            
            if let potentialButton = view as? UIButton
            {
                if let titleString = potentialButton.titleLabel?.text
                {
//                    debugPrint("Original title = \(titleString) Localized button title is \(localizeString(stringToLocalize: titleString))")
                    potentialButton.setTitle(localizeString(stringToLocalize: titleString), for: .normal)
                }
            }
                
            else if let potentialLabel = view as? UILabel
            {
                if potentialLabel.text != nil {
                    //                debugPrint(potentialLabel.text!)
                    potentialLabel.text = localizeString(stringToLocalize: potentialLabel.text!)
                }
            }
            
            else if let potentialImageView = view as? UIImageView
            {
                //Get image name
//                let imageName = potentialImageView.image.
//                potentialImageView.image = UIImage(named: "flag", in: <#T##Bundle?#>, compatibleWith: <#T##UITraitCollection?#>)
            }
            
            else if let potentialTableCell = view as? UITableViewCell {
                potentialTableCell.textLabel?.text = localizeString(stringToLocalize: (potentialTableCell.textLabel?.text)!)
            }
            
            localizeUI(parentView: view)
        }
    }
    
    class func localizeArray(array:[String]) -> [String] //This function takes a base array, and returns the correct localization for it.
    {
        var localizedArray = [String]()
        
        for string in array {
            localizedArray.append(localizeString(stringToLocalize: string))
        }
        
        return localizedArray
    }
}
