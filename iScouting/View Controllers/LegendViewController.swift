//
//  LegendViewController.swift
//  iScouting
//
//  Created by Mohamed Nassar on 08/09/2016.
//  Copyright © 2016 Mohamed Nassar. All rights reserved.
//

import Foundation
import UIKit

class LegendViewController: UIViewController
{
    @IBAction func closeButtonTapped(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
}
