//
//  MainViewController.swift
//  iScouting
//
//  Created by Mohamed Nassar on 4/30/16.
//  Copyright © 2016 Mohamed Nassar. All rights reserved.
//

import Foundation
import UIKit

class MainViewController:UIViewController
{
    let defaults = UserDefaults.standard
    var currentLanguage = UserDefaults.standard.array(forKey: "AppleLanguages")![0] as! String
    
    var supportedLanguagesDictionary = [String:String]()
    
    
    @IBOutlet weak var scoutingButton: UIButton!
    @IBOutlet weak var othersButton: UIButton!
    @IBOutlet weak var languageButton: UIButton!
    @IBOutlet weak var othersView: UIView!
    @IBOutlet weak var playerPassportButton: UIButton!
    @IBOutlet weak var playerCVButton: UIButton!
    @IBOutlet weak var aboutButton: UIButton!
    
   
    
    override func viewDidLoad()
    {
        self.localizeView()
        self.navigationController?.isNavigationBarHidden = true
        
        //Get available language files
        for languageCode in Bundle.main.localizations
        {
            if let languageString = NSLocale.current.localizedString(forLanguageCode: languageCode)
            {
                debugPrint(languageString)
                supportedLanguagesDictionary[languageString] = languageCode
            }
        }
        
    }
    
    //
    //MARK: Under the hood
    //
    
    func localizeView()
    {
        LanguageManager.localizeUI(parentView: self.view)
        
        if currentLanguage.contains("pt")
        {
            languageButton.setImage(UIImage(named:"portugal1"), for: UIControlState())
            scoutingButton.setImage(UIImage(named:"scouting_pt.png"), for: UIControlState())
            othersButton.setImage(UIImage(named:"other_pt.png"), for: UIControlState())
            playerPassportButton.setImage(UIImage(named:"pass_pt.png"), for: UIControlState())
            playerCVButton.setImage(UIImage(named:"cvv_pt.png"), for: UIControlState())
            aboutButton.setImage(UIImage(named:"about_pt.png"), for: UIControlState())
        }
            
        else if currentLanguage.contains("it")
        {
            languageButton.setImage(UIImage(named:"italy1"), for: UIControlState())
            scoutingButton.setImage(UIImage(named:"scouting_it.png"), for: UIControlState())
            othersButton.setImage(UIImage(named:"other_it.png"), for: UIControlState())
            playerPassportButton.setImage(UIImage(named:"pass_it.png"), for: UIControlState())
            playerCVButton.setImage(UIImage(named:"cvv_it.png"), for: UIControlState())
            aboutButton.setImage(UIImage(named:"about_it.png"), for: UIControlState())
        }
            
        else
        {
            languageButton.setImage(UIImage(named:"english1"), for: UIControlState())
            scoutingButton.setImage(UIImage(named:"scout_btn.png"), for: UIControlState())
            othersButton.setImage(UIImage(named:"other_btn.png"), for: UIControlState())
            playerPassportButton.setImage(UIImage(named:"pass_btn.png"), for: UIControlState())
            playerCVButton.setImage(UIImage(named:"cv_btn.png"), for: UIControlState())
            aboutButton.setImage(UIImage(named:"about_btn.png"), for: UIControlState())
        }
    }
    
    //
    //MARK: Interface Interactions
    //

    @IBAction func scoutingButtonTapped(_ sender: UIButton)
    {
        
    }
    
    @IBAction func othersButtonTapped(_ sender: UIButton)
    {
        othersView.isHidden = !othersView.isHidden
    }
    
    @IBAction func languageButtonTapped()
    {
        let languageAlert = UIAlertController(title: LanguageManager.localizeString(stringToLocalize: "Select Application Language"), message: nil, preferredStyle: .alert)
        
        for language in supportedLanguagesDictionary.keys
        {
            let action = UIAlertAction(title: language, style: .default, handler: {
                (action:UIAlertAction) -> Void in
                self.currentLanguage = self.supportedLanguagesDictionary[language]!
                UserDefaults.standard.set([self.currentLanguage], forKey: "AppleLanguages")
                self.localizeView()
            })
            
            languageAlert.addAction(action)
        }
        
//        let englishAction = UIAlertAction(title: "English", style: .default, handler: {
//            (action:UIAlertAction) -> Void in
//            self.currentLanguage = "en"
//            UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
//            self.localizeView()
//        })
//        
//        let italianAction = UIAlertAction(title: "Italian", style: .default, handler: {
//            (action:UIAlertAction) -> Void in
//            self.currentLanguage = "it"
//            UserDefaults.standard.set(["it"], forKey: "AppleLanguages")
//            self.localizeView()
//        })
//        
//        let portogueseAction = UIAlertAction(title: "Portugese", style: .default, handler: {
//            (action:UIAlertAction) -> Void in
//            self.currentLanguage = "pt"
//            UserDefaults.standard.set(["pt"], forKey: "AppleLanguages")
//            self.localizeView()
//        })
//        
//        languageAlert.addAction(englishAction)
//        languageAlert.addAction(italianAction)
//        languageAlert.addAction(portogueseAction)
        
        self.present(languageAlert, animated: true, completion: nil)
    }
}
