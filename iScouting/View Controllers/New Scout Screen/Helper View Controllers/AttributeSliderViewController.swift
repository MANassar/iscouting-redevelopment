//
//  AttributeSliderViewController.swift
//  iScouting
//
//  Created by Mohamed Nassar on 15/07/2017.
//  Copyright © 2017 Mohamed Nassar. All rights reserved.
//

import Foundation
import UIKit

class AttributeSliderViewContoller: UIViewController
{
    @IBOutlet var attributeNameLabel: UILabel!
    @IBOutlet var attributeValueLabel: UILabel!
    @IBOutlet var attributeSlider: UISlider!
    
    var delegate:AttributeSliderViewContollerDelegate?
    var attributeBeingEdited:Attribute?
    var tableIndexBeingEdited:IndexPath?
    var activeAttributesTableView:UITableView?
    
    @IBAction func sliderValueChanged(_ sender: UISlider)
    {
        attributeValueLabel.text = String(format: "%.1f", sender.value)
        delegate?.attributeSliderViewContollerSliderValueDidChange(controller: self, newValue: sender.value)
    }
}

protocol AttributeSliderViewContollerDelegate
{
    func attributeSliderViewContollerSliderValueDidChange(controller:AttributeSliderViewContoller, newValue:Float)
}
