//
//  ClubPickerViewController.swift
//  iScouting
//
//  Created by Mohamed Nassar on 09/04/2017.
//  Copyright © 2017 Mohamed Nassar. All rights reserved.
//

import Foundation
import UIKit

//
// MARK: ClubPickerViewController
//

class ClubPickerViewController:UITableViewController
{
    var delegate:ClubPickerViewControllerDelegate?
    
    let appBundle = Bundle.main
    var countriesXMLPath:String?
    
    var countriesTableView:UITableView!
    var chosenCountryClubsTableView:UITableView!
    
    var countryArray:[[String:String]]?
    var chosenCountryClubArray:[[String:String]]?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        countriesXMLPath = appBundle.path(forResource: "countries", ofType: "xml")
        
        debugPrint(countriesXMLPath!)
        
        var countryXMLData:Data?
        do
        {
            countryXMLData = try Data(contentsOf: URL(fileURLWithPath: countriesXMLPath!))
            let countryDictionary = NSDictionary(xmlData: countryXMLData!)
            
            countryArray = countryDictionary?["periodData"] as? [[String:String]] //This is an Array of Dictionaries where every dictionary is an entry of String and String
        }
            
        catch {
            print("Parsing XML failed");
        }
    }// end viewDidLoad
    
    
    //
    // MARK: Table View DataSource
    //
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return countryArray!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "CountriesCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        let countryDictionary = countryArray?[indexPath.row]
        let countryName = countryDictionary?["Country"]
        
        cell.textLabel?.text = countryName
        
        
        return cell
    }
    
    //
    // MARK: TableView Delegate
    //
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let countryDictionary = countryArray?[indexPath.row]
        var countryCode = countryDictionary!["Code"]
        countryCode = countryCode?.lowercased()
        
        //Now we should parse the XML for the Chosen country
        
        debugPrint("Chosen country code = \(countryCode!)")
        
        let clubXMLPath = appBundle.path(forResource: countryCode!, ofType: "xml")
        
        debugPrint("Club XML path = \(clubXMLPath ?? "Warning: Empty Club XML Path")")
        
        var clubXMLData:Data?
        do
        {
            clubXMLData = try Data(contentsOf: URL(fileURLWithPath: clubXMLPath!))
            let clubDictionary = NSDictionary(xmlData: clubXMLData!)
            
            chosenCountryClubArray = clubDictionary?["periodData"] as? [[String:String]] //This is an Array of Dictionaries where every dictionary is an entry of String and String
            
            self.performSegue(withIdentifier: "ShowClubsForCountry", sender: self)
        }
            
        catch {
            print("Parsing Club XML failed");
        }
    }// end didSelect
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "ShowClubsForCountry"
        {
            if let clubChooser = segue.destination as? ChooseCountryClub
            {
                clubChooser.clubsForCountryArray = chosenCountryClubArray
                clubChooser.delegate = delegate!
            }
        }
        
    }
}

protocol ClubPickerViewControllerDelegate {
    func clubPickerViewControllerDidPick(club:String)
}

//
// MARK: ChooseCountryClub - Helper Class to choose country clubs
//
class ChooseCountryClub: UITableViewController
{
    var clubsForCountryArray:[[String:String]]?
    var delegate:ClubPickerViewControllerDelegate?
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return clubsForCountryArray!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let identifier = "ClubsCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        let clubDictionary = clubsForCountryArray?[indexPath.row]
        let clubName = clubDictionary?["Team"]
        
        cell.textLabel?.text = clubName
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let clubDictionary = clubsForCountryArray?[indexPath.row]
        let clubName = clubDictionary?["Team"]
        
        self.dismiss(animated: true, completion: nil)
        delegate?.clubPickerViewControllerDidPick(club: clubName!)
    }
}
