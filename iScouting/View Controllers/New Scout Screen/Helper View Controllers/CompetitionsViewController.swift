//
//  CompetitionsViewController.swift
//  iScouting
//
//  Created by Mohamed Nassar on 10/04/2017.
//  Copyright © 2017 Mohamed Nassar. All rights reserved.
//

import Foundation
import UIKit

class CompetitionsViewController: UITableViewController, ArraySelectorTableViewControllerDelegate
{
    let appBundle = Bundle.main
    var delegate:CompetitionsViewControllerDelegate?
    
    var internationalCompetitionsXMLPath:String?
    var internationalCompetitionsArray = [String]()
    
    var countriesXMLPath:String?
    var countryArray:[[String:String]]?
    var chosenCountryCompetitionsArray:[[String:String]]?
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        LanguageManager.localizeUI(parentView: self.tableView)
    }
    
    //
    // MARK: Delegate
    //
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch indexPath.row
        {
        case 0:
            debugPrint("Loading international competitions")
//            self.loadInternationaCompetitions()
            
        
        case 1: break
            //load countries
            
        default: break
        }
    }
    
    func arraySelectorTableVCDidSelect(arraySelectorVC: ArraySelectorTableViewController, selection: String) {
        delegate?.competitionsViewController(compVC: self, didPickCompetition: selection)
    }
    
    //
    // MARK: UI Interactions
    //
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if (segue.identifier == "ShowInternationalCompetitions")
        {
            self.loadInternationaCompetitions()
            let arraySelectorVC = segue.destination as! ArraySelectorTableViewController
            arraySelectorVC.delegate = self
            arraySelectorVC.baseArray = internationalCompetitionsArray
        }
        
        else if segue.identifier == "ShowCountries"
        {
            let countryCompVC = segue.destination as! CountryCompetitionsViewController
            countryCompVC.delegate = self.delegate
        }
    }
    
    //
    // Mark: Helper methods
    //
    
    func loadInternationaCompetitions()
    {
        internationalCompetitionsXMLPath = appBundle.path(forResource: "inter_leagues", ofType: "xml")
        
        debugPrint(internationalCompetitionsXMLPath!)
        
        var internationalCompetitionsXMLData:Data?
        do
        {
            internationalCompetitionsXMLData = try Data(contentsOf: URL(fileURLWithPath: internationalCompetitionsXMLPath!))
            let tempDictionary = NSDictionary(xmlData: internationalCompetitionsXMLData!)
            let intCompArrDic = tempDictionary?["periodData"] as? [[String:String]] //This is an Array of Dictionaries where every dictionary is an entry of String and String
            
            for entry in intCompArrDic! {
                internationalCompetitionsArray.append(entry["Visitor"]!)
            }
            
            debugPrint(internationalCompetitionsArray)
        }
            
        catch {
            print("Parsing XML failed");
        }
    }
}

protocol CompetitionsViewControllerDelegate
{
    func competitionsViewController(compVC:AnyObject, didPickCompetition:String)
}


//
//
//

