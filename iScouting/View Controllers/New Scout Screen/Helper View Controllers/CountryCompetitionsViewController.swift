//
//  CountryCompetitionsViewController.swift
//  iScouting
//
//  Created by Mohamed Nassar on 03/06/2017.
//  Copyright © 2017 Mohamed Nassar. All rights reserved.
//

import UIKit

class CountryCompetitionsViewController:UITableViewController, ArraySelectorTableViewControllerDelegate
{
    let appBundle = Bundle.main
    var delegate:CompetitionsViewControllerDelegate?
    
    var countriesXMLPath:String?
    var countryArray:[[String:String]]?
    var competitonsDictionaryArray:[[String:String]]?
    var leaguesForCountry = [String]()
    
    override func viewDidLoad()
    {
        self.loadCountries()
        self.loadLeagues()
    }
    
    func loadCountries()
    {
        countriesXMLPath = appBundle.path(forResource: "countries", ofType: "xml")
        
        debugPrint(countriesXMLPath!)
        
        var countryXMLData:Data?
        do
        {
            countryXMLData = try Data(contentsOf: URL(fileURLWithPath: countriesXMLPath!))
            let countryDictionary = NSDictionary(xmlData: countryXMLData!)
            
            countryArray = countryDictionary?["periodData"] as? [[String:String]] //This is an Array of Dictionaries where every dictionary is an entry of String and String
        }
            
        catch {
            print("Parsing XML failed");
        }
    }
    
    func loadLeagues()
    {
        let countryCompetitionsXML = appBundle.path(forResource: "country_leagues", ofType: "xml")
        
        debugPrint("Country XML path = \(countryCompetitionsXML ?? "Warning: Empty")")
        
        var competitionsXMLData:Data?
        do
        {
            competitionsXMLData = try Data(contentsOf: URL(fileURLWithPath: countryCompetitionsXML!))
            let compDictionary = NSDictionary(xmlData: competitionsXMLData!)
            
            competitonsDictionaryArray = compDictionary?["periodData"] as? [[String:String]] //This is an Array of Dictionaries where every dictionary is an entry of String and String
            
            //            debugPrint(competitonsDictionaryArray)
            //            self.performSegue(withIdentifier: "ShowClubsForCountry", sender: self)
        }
            
        catch {
            print("Parsing Club XML failed");
        }
    }
    
    //
    // MARK: Table View DataSource
    //
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return countryArray!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "CountriesCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        let countryDictionary = countryArray?[indexPath.row]
        let countryName = countryDictionary?["Country"]
        
        cell.textLabel?.text = countryName
        
        return cell
    }
    
    //
    // MARK: TableView Delegate
    //
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let countryDictionary = countryArray?[indexPath.row]
        let countryCode = countryDictionary!["Code"]
        
        debugPrint("Chosen country code = \(countryCode!)")
        
        //Now we will create an array to hold this country's leagues
        leaguesForCountry.removeAll()
        
        for dictionary in competitonsDictionaryArray!
        {
            if dictionary["Code"] == countryCode {
                leaguesForCountry.append(dictionary["League"]!)
            }
        }
        
        debugPrint(leaguesForCountry)
        
        self.performSegue(withIdentifier: "ShowLeaguesForCountry", sender: nil)
        
    }// end didSelect
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if (segue.identifier == "ShowLeaguesForCountry")
        {
            let arrayController = segue.destination as! ArraySelectorTableViewController
            arrayController.baseArray = leaguesForCountry
            arrayController.delegate = self
        }
    }
    
    func arraySelectorTableVCDidSelect(arraySelectorVC: ArraySelectorTableViewController, selection: String)
    {
        debugPrint(selection)
        delegate?.competitionsViewController(compVC: self, didPickCompetition: selection)
    }
}
