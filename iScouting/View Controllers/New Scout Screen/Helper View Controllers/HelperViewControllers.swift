//
//  HelperViewControllers.swift
//  iScouting
//
//  Created by Mohamed Nassar on 08/01/2017.
//  Copyright © 2017 Mohamed Nassar. All rights reserved.
//

import Foundation
import UIKit
import MICountryPicker

//
// MARK: Helper view controllers
//

class DatePickerViewController: UIViewController
{
    
    var chosenDate:Date?
    var delegate:DatePickerViewControllerDelegate?
    
    @IBOutlet var datePicker: UIDatePicker!
    
    @IBAction func doneButtonTapped(_ sender: UIBarButtonItem)
    {
        chosenDate = datePicker.date
        delegate?.datePickerViewControllerDidFinishWithDate(date: chosenDate!)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIBarButtonItem)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

protocol DatePickerViewControllerDelegate {
    func datePickerViewControllerDidFinishWithDate (date:Date)
}

class PositionPickerViewController: UITableViewController
{
    var positions:[String]!
    var positionRecepient:UITextField?
    var delegate:PositionPickerViewControllerDelegate?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        positions = [LanguageManager.localizeString(stringToLocalize: "GK"),  LanguageManager.localizeString(stringToLocalize: "RB"),  LanguageManager.localizeString(stringToLocalize: "RWB"),  LanguageManager.localizeString(stringToLocalize: "CB"),  LanguageManager.localizeString(stringToLocalize: "SWEEPER"),  LanguageManager.localizeString(stringToLocalize: "LB"),  LanguageManager.localizeString(stringToLocalize: "LWB"),  LanguageManager.localizeString(stringToLocalize: "DM"),  LanguageManager.localizeString(stringToLocalize: "DLP"),  LanguageManager.localizeString(stringToLocalize: "RM"),  LanguageManager.localizeString(stringToLocalize: "CM"),  LanguageManager.localizeString(stringToLocalize: "LM"),  LanguageManager.localizeString(stringToLocalize: "RAM"),  LanguageManager.localizeString(stringToLocalize: "AM"),  LanguageManager.localizeString(stringToLocalize: "LAM"),  LanguageManager.localizeString(stringToLocalize: "RW"),  LanguageManager.localizeString(stringToLocalize: "RF"),  LanguageManager.localizeString(stringToLocalize: "SS"),  LanguageManager.localizeString(stringToLocalize: "CF"),  LanguageManager.localizeString(stringToLocalize: "LW"),  LanguageManager.localizeString(stringToLocalize: "LF")]
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int { return 1 }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return positions.count }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PositionCell")
        cell?.textLabel?.text = positions[indexPath.row]
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.positionPickerViewControllerDidPick(position: positions[indexPath.row])
    }
}

protocol PositionPickerViewControllerDelegate {
    func positionPickerViewControllerDidPick(position: String)
}

//
// MARK: Score Controller
//

class ScoreViewController: UIViewController
{
    @IBOutlet var homeTeamLabel: UILabel!
    @IBOutlet var awayTeamLabel: UILabel!
    @IBOutlet var homeTeamScoreTextField: UITextField!
    @IBOutlet var awayTeamScoreTextField: UITextField!
    @IBOutlet var homeTeamScoreStepper: UIStepper!
    @IBOutlet var awayTeamScoreStepper: UIStepper!
    
    var delegate:ScoreViewControllerDelegate?
    
    @IBAction func stepperValueChanged(_ sender: UIStepper)
    {
        if sender == homeTeamScoreStepper {
            homeTeamScoreTextField.text = String(Int(homeTeamScoreStepper.value))
        }
        
        else if sender == awayTeamScoreStepper {
            awayTeamScoreTextField.text = String(Int(awayTeamScoreStepper.value))
        }
        
        delegate?.scoreViewControllerDidChangeScore(homeTeamScore: Int(homeTeamScoreStepper.value), awayTeamScore: Int(awayTeamScoreStepper.value))
    }
    
}

protocol ScoreViewControllerDelegate {
    func scoreViewControllerDidChangeScore(homeTeamScore:Int, awayTeamScore:Int)
}
