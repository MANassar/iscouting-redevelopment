//
//  NewScoutViewController.swift
//  iScouting
//
//  Created by Mohamed Nassar on 7/4/16.
//  Copyright © 2016 Mohamed Nassar. All rights reserved.
//

import UIKit
import MICountryPicker
import EVReflection
import Realm
import RealmSwift




class NewScoutViewController: UIViewController
{
    //
    //MARK: Outlets
    //
    @IBOutlet var playerImageContainerView: UIView!
    @IBOutlet var personalDetailsAView: UIView!
    @IBOutlet var personalDetailsBView: UIView!
    
    //
    // Personal Details A
    //
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var surnameTextField: UITextField!
    @IBOutlet var nationalityTextField: UITextField!
    @IBOutlet var nationalityButton: UIButton!
    @IBOutlet var dateOfBirthTextField: UITextField!
    @IBOutlet var dateOfBirthButton: UIButton!
    @IBOutlet var positionTextField: UITextField!
    @IBOutlet var positionAltTextField: UITextField!
    
    //
    // Personal Details B
    //
    @IBOutlet var heightTextField: UITextField!
    @IBOutlet var weightTextField: UITextField!
    @IBOutlet var buildTextField: UITextField!
    @IBOutlet var footTextField: UITextField!
    @IBOutlet var shirtTextField: UITextField!
    @IBOutlet var videoTextField: UITextField!
    
    @IBOutlet var currentClubTextField: UITextField!
    @IBOutlet var currentClubButton: UIButton!
    @IBOutlet var previousClubTextField: UITextField!
    @IBOutlet var previousClubButton: UIButton!
    @IBOutlet var contractExpiryTextField: UITextField!
    @IBOutlet var contractExpiryDateButton: UIButton!
    @IBOutlet var salaryTextField: UITextField!
    @IBOutlet var playerAgentTextField: UITextField!
    @IBOutlet var injuriesTextField: UITextField!
    
    var dateReciepient: UITextField? //When a date is requested, this variable will point to the text field that should recieve it.
    var countryReciepient:UITextField?
    var positionReciepient:UITextField?
    var clubRecipientTextField:UITextField?
    
    var embeddedTabBarController:UITabBarController?
    var playerAttributesViewController:PlayerAttributesViewController?
    var liveScoutingViewController:LiveScoutingViewController?
    var additionalInfoViewController:AdditionalInfoViewController?
    var datePickerViewController:DatePickerViewController?
    
    var mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    var realm = try! Realm()
    
    //
    // MARK: View Lifecycle
    //
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let borderColor = UIColor.darkGray.cgColor
        let borderWidth:CGFloat = 1.5
        let cornerRadius:CGFloat = 6
        
        playerImageContainerView.layer.borderColor = borderColor
        playerImageContainerView.layer.borderWidth = borderWidth
        playerImageContainerView.layer.cornerRadius = cornerRadius
        
        personalDetailsAView.layer.borderColor = borderColor
        personalDetailsAView.layer.borderWidth = borderWidth
        personalDetailsAView.layer.cornerRadius = cornerRadius
        
        personalDetailsBView.layer.borderColor = borderColor
        personalDetailsBView.layer.borderWidth = borderWidth
        personalDetailsBView.layer.cornerRadius = cornerRadius
        
        LanguageManager.localizeUI(parentView: self.view)
    }
    
    
    
    @IBAction func closeButtonTapped(_ sender: UIBarButtonItem)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "embedTabBarController"
        {
            self.embeddedTabBarController = segue.destination as? UITabBarController
            self.playerAttributesViewController = embeddedTabBarController?.viewControllers![0] as? PlayerAttributesViewController
            self.liveScoutingViewController = embeddedTabBarController?.viewControllers![1] as? LiveScoutingViewController
            self.additionalInfoViewController = embeddedTabBarController?.viewControllers![2] as? AdditionalInfoViewController
        }
        
        else if segue.identifier == "PresentPositionPicker"
        {
            let positionPickerVC = segue.destination as! PositionPickerViewController
            positionPickerVC.delegate = self
            self.positionReciepient = positionTextField
        }
        
        else if segue.identifier == "PresentAltPositionPicker"
        {
            let positionPickerVC = segue.destination as! PositionPickerViewController
            positionPickerVC.delegate = self
            self.positionReciepient = positionAltTextField
        }
        
        else if segue.identifier == "ShowCurrentClubPicker" || segue.identifier == "ShowPreviousClubPicker"
        {
            let navController = segue.destination as! UINavigationController
            if let clubPicker = navController.topViewController as? ClubPickerViewController
            {
                clubPicker.delegate = self
                
                if segue.identifier == "ShowCurrentClubPicker" { clubRecipientTextField = currentClubTextField }
                else { clubRecipientTextField = previousClubTextField }
            }
        }
    }
    
    //
    // MARK: UIInteraction
    //
    
    @IBAction func showDatePicker(_ sender: UIButton)
    {
        datePickerViewController = mainStoryboard.instantiateViewController(withIdentifier: "datePickerVC") as? DatePickerViewController
        datePickerViewController?.modalPresentationStyle = .popover
        datePickerViewController?.popoverPresentationController?.sourceView = sender
        datePickerViewController?.delegate = self
        
        if sender == dateOfBirthButton {
            dateReciepient = dateOfBirthTextField
        }
        
        else if sender == contractExpiryDateButton {
            dateReciepient = contractExpiryTextField
        }
        
        self.present(datePickerViewController!, animated: true, completion: nil)
    }
    
    @IBAction func nationalityButton(_ sender: UIButton)
    {
        let nationalityPicker = MICountryPicker()
        nationalityPicker.modalPresentationStyle = .popover
        nationalityPicker.popoverPresentationController?.sourceView = sender
        nationalityPicker.delegate = self
        
        if sender == nationalityButton {
            countryReciepient = nationalityTextField
        }
        
        self.present(nationalityPicker, animated: true, completion: nil)
    }
    
    
}


//
// Extention to hold delegate calls. Just for code organization
//

extension NewScoutViewController:DatePickerViewControllerDelegate, MICountryPickerDelegate, PositionPickerViewControllerDelegate, ClubPickerViewControllerDelegate
{
    func datePickerViewControllerDidFinishWithDate(date: Date)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        
        dateReciepient?.text = dateFormatter.string(from: date)
    }
    
    func countryPicker(_ picker: MICountryPicker, didSelectCountryWithName name: String, code: String)
    {
        self.dismiss(animated: true, completion: nil)
        countryReciepient?.text = name
    }
    
    func positionPickerViewControllerDidPick(position: String)
    {
        self.positionReciepient?.text = position
    }
    
    func clubPickerViewControllerDidPick(club: String)
    {
        clubRecipientTextField?.text = club
    }
}



