//
//  PlayerAttributesViewController.swift
//  iScouting
//
//  Created by Mohamed Nassar on 15/07/2017.
//  Copyright © 2017 Mohamed Nassar. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class PlayerAttributesViewController:UIViewController
{
    //
    // MARK: Outlets
    //
    
    @IBOutlet var matchDetailsDateButton: UIButton!
    @IBOutlet var matchDetailsHomeTeamButton: UIButton!
    @IBOutlet var matchDetailsAwayTeamButton: UIButton!
    @IBOutlet var matchDetailsScoreButton: UIButton!
    @IBOutlet var matchDetailsCompetitionButton: UIButton!
    @IBOutlet var matchDetailsWeatherButton: UIButton!
    @IBOutlet var matchDetailsPitchConditionButton: UIButton!
    
    //Physical Attributes subview
    @IBOutlet var physicalAttributesTableView: UITableView!
    @IBOutlet var tacticalAttributesTableView: UITableView!
    @IBOutlet var technicalAttributesTableView: UITableView!
    @IBOutlet var psychoSocialAttributesTableView: UITableView!
    
    var realm = try! Realm()
    
    var clubChooserRecepientButton:UIButton?
    var tableDataSourceArray:Results<Attribute>?
    
    var physicalAttributes:Results<Attribute>!
    var tacticalAttributes:Results<Attribute>!
    var technicalAttributes:Results<Attribute>!
    var psychoSocialAttributes:Results<Attribute>!
    
    override func viewDidLoad()
    {
        LanguageManager.localizeUI(parentView: self.view)
        
        
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        
        //Realm interaction
        
        physicalAttributes = realm.objects(Attribute.self).filter("attributeTypeInt = 0")
        tacticalAttributes = realm.objects(Attribute.self).filter("attributeTypeInt = 1")
        technicalAttributes = realm.objects(Attribute.self).filter("attributeTypeInt = 2")
        psychoSocialAttributes = realm.objects(Attribute.self).filter("attributeTypeInt = 3")
        
        //Check and load the attributes from Realm
        
        if (physicalAttributes.count == 0)
        {
            debugPrint("No physical attributes defined. Create them")
            
            let physicalAttributesArray = [
                Attribute(defaultName: "Strength", value: 0.0, type: AttributeType.Physical, id: 0),
                Attribute(defaultName: "Power", value: 0.0, type: AttributeType.Physical, id: 1),
                Attribute(defaultName: "Pace", value: 0.0, type: AttributeType.Physical, id: 2),
                Attribute(defaultName: "Stamina", value: 0.0, type: AttributeType.Physical, id: 3),
                Attribute(defaultName: "Agility", value: 0.0, type: AttributeType.Physical, id: 4),
                Attribute(defaultName: "Flexibility", value: 0.0, type: AttributeType.Physical, id: 5),
                Attribute(defaultName: "Coordination", value: 0.0, type: AttributeType.Physical, id: 6),
                Attribute(defaultName: "Work Rate", value: 0.0, type: AttributeType.Physical, id: 7),
                Attribute(defaultName: "Reaction", value: 0.0, type: AttributeType.Physical, id: 8),
                Attribute(defaultName: "Acceleration", value: 0.0, type: AttributeType.Physical, id: 9)]
            
            try! realm.write {
                debugPrint("Writing to realm")
                realm.add(physicalAttributesArray)
            }
        }
        
        if (tacticalAttributes.count == 0)
        {
            debugPrint("No tactical attributes defined. Create them")
            
            let tacticalAttributesArray = [
                Attribute(defaultName: "Positioning", value: 0.0, type: AttributeType.Tactical, id: 10),
                Attribute(defaultName: "Anticipating", value: 0.0, type: AttributeType.Tactical, id: 11),
                Attribute(defaultName: "Decision making", value: 0.0, type: AttributeType.Tactical, id: 12),
                Attribute(defaultName: "Vision", value: 0.0, type: AttributeType.Tactical, id: 13),
                Attribute(defaultName: "Awareness", value: 0.0, type: AttributeType.Tactical, id: 14),
                Attribute(defaultName: "Off the ball", value: 0.0, type: AttributeType.Tactical, id: 15),
                Attribute(defaultName: "1 vs 1 Attack", value: 0.0, type: AttributeType.Tactical, id: 16),
                Attribute(defaultName: "1 vs 1 Defence", value: 0.0, type: AttributeType.Tactical, id: 17),
                Attribute(defaultName: "Set pieces attack", value: 0.0, type: AttributeType.Tactical, id: 18),
                Attribute(defaultName: "Set pieces defence", value: 0.0, type: AttributeType.Tactical, id: 19)]
            
            try! realm.write {
                debugPrint("Writing to realm")
                realm.add(tacticalAttributesArray)
            }
        }
        
        if (technicalAttributes.count == 0)
        {
            debugPrint("No technical attributes defined. Create them")
            
            let technicalAttributesArray = [
                Attribute(defaultName: "First touch", value: 0.0, type: AttributeType.Technical, id: 20),
                Attribute(defaultName: "Weaker foot", value: 0.0, type: AttributeType.Technical, id: 21),
                Attribute(defaultName: "Passing", value: 0.0, type: AttributeType.Technical, id: 22),
                Attribute(defaultName: "Shooting", value: 0.0, type: AttributeType.Technical, id: 23),
                Attribute(defaultName: "Heading", value: 0.0, type: AttributeType.Technical, id: 24),
                Attribute(defaultName: "Dribbling", value: 0.0, type: AttributeType.Technical, id: 25),
                Attribute(defaultName: "Finishing", value: 0.0, type: AttributeType.Technical, id: 26),
                Attribute(defaultName: "Tackling", value: 0.0, type: AttributeType.Technical, id: 27),
                Attribute(defaultName: "Marking", value: 0.0, type: AttributeType.Technical, id: 28),
                Attribute(defaultName: "Covering", value: 0.0, type: AttributeType.Technical, id: 29)]
            
            try! realm.write {
                debugPrint("Writing to realm")
                realm.add(technicalAttributesArray)
            }
        }
        
        if (psychoSocialAttributes.count == 0)
        {
            debugPrint("No psycho-social attributes defined. Create them")
            
            let psychoSocialAttributesArray = [
                Attribute(defaultName: "Confidence", value: 0.0, type: AttributeType.Psycho_Social, id: 30),
                Attribute(defaultName: "Attitude", value: 0.0, type: AttributeType.Psycho_Social, id: 31),
                Attribute(defaultName: "Bravery", value: 0.0, type: AttributeType.Psycho_Social, id: 32),
                Attribute(defaultName: "Creativity", value: 0.0, type: AttributeType.Psycho_Social, id: 33),
                Attribute(defaultName: "Concentration", value: 0.0, type: AttributeType.Psycho_Social, id: 34),
                Attribute(defaultName: "Leadership", value: 0.0, type: AttributeType.Psycho_Social, id: 35),
                Attribute(defaultName: "Determination", value: 0.0, type: AttributeType.Psycho_Social, id: 36),
                Attribute(defaultName: "Team work", value: 0.0, type: AttributeType.Psycho_Social, id: 37),
                Attribute(defaultName: "Composure", value: 0.0, type: AttributeType.Psycho_Social, id: 38),
                Attribute(defaultName: "Communication", value: 0.0, type: AttributeType.Psycho_Social, id: 39)]
            
            try! realm.write {
                debugPrint("Writing to realm")
                realm.add(psychoSocialAttributesArray)
            }
        }
        
        //Reload them in case we didnt get them before
        
        physicalAttributes = realm.objects(Attribute.self).filter("attributeTypeInt = 0")
        tacticalAttributes = realm.objects(Attribute.self).filter("attributeTypeInt = 1")
        technicalAttributes = realm.objects(Attribute.self).filter("attributeTypeInt = 2")
        psychoSocialAttributes = realm.objects(Attribute.self).filter("attributeTypeInt = 3")
    }
    
    //
    // MARK: UIInteraction methods
    //
    
    @IBAction func matchDetailsDateButtonTapped(_ sender: UIButton)
    {
        let datePickerViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "datePickerVC") as? DatePickerViewController
        datePickerViewController?.modalPresentationStyle = .popover
        datePickerViewController?.popoverPresentationController?.sourceView = sender
        datePickerViewController?.popoverPresentationController?.permittedArrowDirections = .down
        datePickerViewController?.delegate = self
        
        self.present(datePickerViewController!, animated: true, completion: nil)
    }
    
    
    //
    // Segue handling
    //
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "ShowHomeTeamPicker" || segue.identifier == "ShowAwayTeamPicker"
        {
            let navController = segue.destination as! UINavigationController
            if let clubPicker = navController.topViewController as? ClubPickerViewController
            {
                clubPicker.delegate = self
                
                if segue.identifier == "ShowHomeTeamPicker" {
                    clubChooserRecepientButton = matchDetailsHomeTeamButton
                }
                    
                else {
                    clubChooserRecepientButton = matchDetailsAwayTeamButton
                }
                
            }
        }
            
        else if segue.identifier == "ShowScoreVC"
        {
            let scoreVC = segue.destination as! ScoreViewController
            scoreVC.delegate = self
        }
            
        else if segue.identifier == "ShowWeatherVC"
        {
            let weatherArray = ["N/A", "Sunny", "Clear", "Overcast", "Partly cloudy", "Mostly cloudy", "Scattered clouds", "Mist", "Fog", "Hail", "Drizzle", "Rainy", "Light rain", "Heavy rain", "Thunderstorm", "Light thunderstorms and rain"]
            let arraySelectorVC = segue.destination as! ArraySelectorTableViewController
            arraySelectorVC.delegate = self
            arraySelectorVC.reciever = matchDetailsWeatherButton
            arraySelectorVC.baseArray = weatherArray
        }
            
        else if (segue.identifier == "ShowCompetitionChooser")
        {
            let navController = segue.destination as! UINavigationController
            if let competitionPicker = navController.topViewController as? CompetitionsViewController
            {
                competitionPicker.delegate = self
            }
        }
    }
}//end class

//
// Extension to hold table view stuff
//
extension PlayerAttributesViewController: UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (section == 1) { //Comments section in all tables
            return 1
        }
            
        else
        {
            //Which table view are we talking about? Granted they all have the same number of rows, but we have to be on the safe side.
            if (tableView == physicalAttributesTableView) {
                tableDataSourceArray = physicalAttributes
            }
                
            else if (tableView == tacticalAttributesTableView) {
                tableDataSourceArray = tacticalAttributes
            }
            
            else if (tableView == technicalAttributesTableView) {
                tableDataSourceArray = technicalAttributes
            }
            
            else if (tableView == psychoSocialAttributesTableView) {
                tableDataSourceArray = psychoSocialAttributes
            }
            
            return tableDataSourceArray!.count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        if section == 1 {
            return LanguageManager.localizeString(stringToLocalize: "Comments")
        }
            
        else {
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell:UITableViewCell?
        
        if (indexPath.section == 1) {
            cell = tableView.dequeueReusableCell(withIdentifier: "CommentsCell", for: indexPath)
        }
            
        else
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "AttributeCell", for: indexPath)
            
            if let currentAttribute = tableDataSourceArray?[indexPath.row]
            {
                cell?.textLabel?.text = LanguageManager.localizeString(stringToLocalize: currentAttribute.customName)
                cell?.detailTextLabel?.text = String(format: "%.1f", currentAttribute.value)
            }
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        debugPrint("Selected row \(indexPath.row)")
        
        var attributesArray: Results<Attribute>!
        
        if (tableView == physicalAttributesTableView) {
            attributesArray = physicalAttributes
        }
            
        else if (tableView == tacticalAttributesTableView) {
            attributesArray = tacticalAttributes
        }
            
        else if (tableView == technicalAttributesTableView) {
            attributesArray = technicalAttributes
        }
            
        else if (tableView == psychoSocialAttributesTableView) {
            attributesArray = psychoSocialAttributes
        }
        
        let selectedAttribute = attributesArray[indexPath.row]
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        if let attributesSlider = storyBoard.instantiateViewController(withIdentifier: "attributeSliderVC") as? AttributeSliderViewContoller
        {
            attributesSlider.modalPresentationStyle = .popover
            attributesSlider.popoverPresentationController?.sourceView = tableView.cellForRow(at: indexPath)
            attributesSlider.loadView()
            attributesSlider.attributeNameLabel.text = selectedAttribute.customName
            attributesSlider.attributeValueLabel.text = String(describing: selectedAttribute.value)
            attributesSlider.delegate = self
            attributesSlider.attributeBeingEdited = selectedAttribute
            attributesSlider.tableIndexBeingEdited = indexPath
            attributesSlider.activeAttributesTableView = tableView
            
            self.present(attributesSlider, animated: true, completion: nil)
        }
        
    }
} //end Table stuff extension

//
// Extension to hold Delegate handlers
//
extension PlayerAttributesViewController: DatePickerViewControllerDelegate, ClubPickerViewControllerDelegate, ScoreViewControllerDelegate, ArraySelectorTableViewControllerDelegate, CompetitionsViewControllerDelegate, AttributeSliderViewContollerDelegate
{
    func datePickerViewControllerDidFinishWithDate(date: Date)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        
        matchDetailsDateButton.setTitle(dateFormatter.string(from: date), for: .normal)
    }
    
    
    func clubPickerViewControllerDidPick(club: String)
    {
        clubChooserRecepientButton?.setTitle(club, for: .normal)
    }
    
    func scoreViewControllerDidChangeScore(homeTeamScore:Int, awayTeamScore:Int)
    {
        let scoreString = "\(homeTeamScore) : \(awayTeamScore)"
        matchDetailsScoreButton.setTitle(scoreString, for: .normal)
    }
    
    func arraySelectorTableVCDidSelect(arraySelectorVC:ArraySelectorTableViewController, selection:String)
    {
        if let potentialButton = arraySelectorVC.reciever as? UIButton {
            potentialButton.setTitle(selection, for: .normal)
        }
            
        else if let potentialLabel = arraySelectorVC.reciever as? UILabel {
            potentialLabel.text = selection
        }
    }
    
    func competitionsViewController(compVC: AnyObject, didPickCompetition: String) {
        self.matchDetailsCompetitionButton.setTitle(didPickCompetition, for: .normal)
    }
    
    func attributeSliderViewContollerSliderValueDidChange(controller: AttributeSliderViewContoller, newValue:Float)
    {
        //Which table view are we talking about? Granted they all have the same number of rows, but we have to be on the safe side.
        if (controller.activeAttributesTableView == physicalAttributesTableView) {
            tableDataSourceArray = physicalAttributes
        }
        
        else if (controller.activeAttributesTableView == tacticalAttributesTableView) {
            tableDataSourceArray = tacticalAttributes
        }
        
        else if (controller.activeAttributesTableView == technicalAttributesTableView) {
            tableDataSourceArray = technicalAttributes
        }
        
        else if (controller.activeAttributesTableView == psychoSocialAttributesTableView) {
            tableDataSourceArray = psychoSocialAttributes
        }
        
        let row = controller.tableIndexBeingEdited?.row
        let attribute = tableDataSourceArray?[row!]
        
        try! realm.write {
            attribute?.value = newValue
        }
        
        controller.activeAttributesTableView?.reloadRows(at: [controller.tableIndexBeingEdited!], with: .automatic)
        
    }
}
