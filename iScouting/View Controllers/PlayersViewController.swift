//
//  PlayersViewController.swift
//  iScouting
//
//  Created by Mohamed Nassar on 7/1/16.
//  Copyright © 2016 Mohamed Nassar. All rights reserved.
//

import UIKit

class PlayersViewController: UIViewController//, UITableViewDelegate, UITableViewDataSource
{

    weak var delegate:PlayersViewControllerPlayerDelegate?
    
    //
    //MARK: View Controller Lifecycle
    //
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
    }
    
    
    //
    // MARK: Table View Datasource
    //
    
//    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        
//    }
    
    //
    // MARK: User Interactions
    //
    
    @IBAction func closeButtonTapped(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

protocol PlayersViewControllerPlayerDelegate:class
{
    func playerSelected(_ newPlayer:Player)
}
